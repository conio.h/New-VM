New-VM.ps1
===========

A useful wrapper (for me) to create new Hyper-V VMs from Windows 10 ISOs in a single command.

```powershell
# Creates a new Hyper-V VM with 2 cores, 4GB RAM and a 120GB disk and applies a
# WIM image onto the disk.

.\New-VM.ps1 -Name "Windows 10 Enterprise 20H2 x64" -ISOPath `
    "path\to\en_windows_10_business_editions_version_20h2_x64_dvd_4788fb7c.iso" `
    -ImageName "Windows 10 Enterprise"

# You can specify those parameters instead of taking the defaults.
# You can also use -WIMPath instead of -ISOPath (perhaps if you've  got a WIM you
# sysprepped) and use -ImageIndex instead of -ImageName.
```


Table of contents
=================

- [Features](#Features)
  - [Limitations](#Limitations)
  - [Windows 7 support](#Windows-7-support)
- [Syntax](#Syntax)


Features
========

- Parameters for CPU count, RAM size, disk size and virtual switch name.
  - With defaults I set without thinking too hard about it.
- Installs Windows from an ISO or directly from a WIM file.
- Supports edition name instead of a WIM index.
- Puts the initial VHDX in the right place, without you having to pass a full path for Microsoft's `New-VM ... -NewVHDPath _nope_`.
- Changes Microsoft's `New-VM` annoying defaults:
  - Disables automatic checkpoints and automatic start action.
- Supports `unattend.xml`
  - Has a basic `unattend.xml` file to skip most of the OOBE.
  - Supports a custom file using a parameter (so you can run two operations simultaneously using different files).
  - The file has a few commented out sections that I found useful but couldn't supply public defaults:
    - License key
    - Computer name
    - Initial users (If you add that one it skips OOBE entirely.)
- Creates a snapshot after finishing (optional default).


Limitations
-----------

- No error checking. Any error checking that is present is a bonus.
- Only accesses the local Hyper-V server. (Shouldn't be hard to add.)
- Requires to be run as an administrator, because DISM requires it to extract the WIM image. No, I'm not going to use [wimlib](https://wimlib.net) or whatever.
- Doesn't even try to be generic:
  - Hardcoded: Assumes there's a WIM file at `ISO\sources\install.wim`
  - Can't set VM path, VHD path, smart pagefile path, etc.
- New users begin in light mode instead of dark mode. :slightly_frowning_face:


Windows 7 support
-----------------

Nobody should care about Windows 7, but some people unfortunately are still forced to.

Windows 7 (and 2008 and 2008 R2) support is minimal, but exists. You'll have to specify all the correct flags on your own, though. The script doesn't make any attempts to realize you're providing it with a Windows 7 ISO/WIM and chnage its defaults.

You should set definitely set `-Generation 1` and `-PartitionStyle MBR` and also either set `-UnattendXMLPath ""` or change the attached `unattend.xml` file to work on Windows 7.

```powershell
# This works:
.\New-VM.ps1 -Name "Windows 7 Enterprise" `
    -ISOPath "path\to\en_windows_7_enterprise_with_sp1_x64_dvd_u_677651.iso" `
    -ImageName "Windows 7 Enterprise" -Generation 1 -PartitionStyle MBR `
    -UnattendXMLPath ""
```

The same goes for 32-bit versions. You must specify `-Generation 1`.


Syntax
======

You must supply (a) a name for the VM; (b) a path to either an ISO or a WIM, and (c) an image name or index in the WIM file.

Everything else is optional.

To prevent a snapshot from being created pass `-NoSnapshot`.  
To get a VM object back from the script use `-Passthru`.

If you don't want the defaults for the VMSwitch or the `unattend.xml` file pass an empty string:

- `-VMSwitchName ""` - keep the virtual network adapter disconnected.
- `-UnattendXMLPath ""` - don't use `unattend.xml` at all.

```powershell
New-VM.ps1 -Name <string> -ISOPath <string> -ImageName <string> [-Generation <int>]
           [-CPUCores <int>] [-MemorySize <long>] [-DiskSize <long>] [-PartitionStyle
           <string>] [-VMSwitchName <string>] [-UnattendXMLPath <string>] [-NoSnapshot]
           [-Passthru] [<CommonParameters>]

New-VM.ps1 -Name <string> -ISOPath <string> -ImageIndex <int> [-Generation <int>]
           [-CPUCores <int>] [-MemorySize <long>] [-DiskSize <long>] [-PartitionStyle
           <string>] [-VMSwitchName <string>] [-UnattendXMLPath <string>] [-NoSnapshot]
           [-Passthru] [<CommonParameters>]

New-VM.ps1 -Name <string> -WIMPath <string> -ImageName <string> [-Generation <int>]
           [-CPUCores <int>] [-MemorySize <long>] [-DiskSize <long>] [-PartitionStyle
           <string>] [-VMSwitchName <string>] [-UnattendXMLPath <string>] [-NoSnapshot]
           [-Passthru] [<CommonParameters>]

New-VM.ps1 -Name <string> -WIMPath <string> -ImageIndex <int> [-Generation <int>]
           [-CPUCores <int>] [-MemorySize <long>] [-DiskSize <long>] [-PartitionStyle
           <string>] [-VMSwitchName <string>] [-UnattendXMLPath <string>] [-NoSnapshot]
           [-Passthru] [<CommonParameters>]
```
