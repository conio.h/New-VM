#Requires -RunAsAdministrator
# Because DISM sucks

[CmdletBinding(PositionalBinding=$false)]
Param (
    [Parameter(Mandatory = $True)][string]$Name,
    [ValidateSet(1,2)][int]$Generation = 2,
    [int]$CPUCores = 2,
    [Int64]$MemorySize = 4GB,
    [Int64]$DiskSize = 120GB,
    [ValidateSet("GPT","MBR")][string]$PartitionStyle = "GPT",
    [string]$VMSwitchName = "Default Switch",
    [string]$UnattendXMLPath = $(Join-Path $PSScriptRoot "unattend.xml"),
    [Parameter(ParameterSetName = 'ISOIndex', Mandatory = $True)]
    [Parameter(ParameterSetName = 'ISOName', Mandatory = $True)]
    [string]$ISOPath,
    [Parameter(ParameterSetName = 'WIMIndex', Mandatory = $True)]
    [Parameter(ParameterSetName = 'WIMName', Mandatory = $True)]
    [string]$WIMPath,
    [Parameter(ParameterSetName = 'ISOName', Mandatory = $True)]
    [Parameter(ParameterSetName = 'WIMName', Mandatory = $True)]
    [string]$ImageName,
    [Parameter(ParameterSetName = 'ISOIndex', Mandatory = $True)]
    [Parameter(ParameterSetName = 'WIMIndex', Mandatory = $True)]
    [int]$ImageIndex,
    [Switch]$NoSnapshot,
    [Switch]$Passthru
)

Set-StrictMode -Version Latest

Set-Variable -Option Constant -Name HYPERV_CONFIG_KEY_PATH -Value "Registry::HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Virtualization"
Set-Variable -Option Constant -Name GPT_DISK_TYPE_ESP -Value "{c12a7328-f81f-11d2-ba4b-00a0c93ec93b}"


Function New-VM-Impl {
    [CmdletBinding(PositionalBinding=$false)]
    Param (
        [Parameter(Mandatory = $True)][string]$Name,
        [Parameter(Mandatory = $True)][ValidateSet(1,2)][int]$Generation,
        [Parameter(Mandatory = $True)][int]$CPUCores,
        [Parameter(Mandatory = $True)][Int64]$MemorySize,
        [Parameter(Mandatory = $True)][Int64]$DiskSize,
        [Parameter(Mandatory = $True)][ValidateSet("GPT","MBR")][string]$PartitionStyle,
        [Parameter(Mandatory = $True)][AllowEmptyString()][string]$VMSwitchName,
        [Parameter(Mandatory = $True)][AllowEmptyString()][string]$UnattendXMLPath,
        [Parameter(Mandatory = $True)][string]$WIMPath,
        [Parameter(Mandatory = $True)][int]$ImageIndex,
        [Parameter(Mandatory = $True)][Switch]$NoSnapshot,
        [Parameter(Mandatory = $True)][Switch]$Passthru
    )
    
    $VHDFolder = Get-ItemPropertyValue $HYPERV_CONFIG_KEY_PATH -Name "DefaultVirtualHardDiskPath"
    $VHDFilename = Join-Path $VHDFolder ($Name + " - OSDisk.vhdx")
    
    Write-Verbose "VHDX file name: $VHDFilename"

    $ArgsToNewVM = @{ Generation = $Generation; BootDevice = "VHD"; `           # will magically mean "IDE" on Gen1 VMs
                      Name = $Name; MemoryStartupBytes = $MemorySize; SwitchName = $VMSwitchName; `
                      NewVHDPath = $VHDFilename; NewVHDSizeBytes = $DiskSize }
    
    If ($ArgsToNewVM["SwitchName"] -EQ "") {
        $ArgsToNewVM.Remove("SwitchName")
    }
    
    $ArgsToNewVM | Out-String | Write-Debug

    Write-Host "Creating VM..."
    $VM = New-VM @ArgsToNewVM
    
    Set-VM -VM $VM -ProcessorCount $CPUCores -AutomaticCheckpointsEnabled $false -AutomaticStartAction Nothing

    Write-Host "Mounting VHD..."
    $MountedVHD = Mount-DiskImage -ImagePath $VHDFilename

    Try {
        $MountedVHDDisk = $MountedVHD | Get-Disk

        Write-Host "Initializing $PartitionStyle disk..."
        Initialize-Disk -PartitionStyle $PartitionStyle -InputObject $MountedVHDDisk
        
        if ($PartitionStyle -EQ "GPT") {
            Write-Host "Preparing ESP partition"
            $ESP = $MountedVHDDisk | New-Partition -AssignDriveLetter -Size 350MB -GptType $GPT_DISK_TYPE_ESP 
            $ESP | Format-Volume -FileSystem FAT32 -NewFileSystemLabel "System" | Out-Null
            $SystemDriveLetter = $ESP.DriveLetter + ":\"
            Write-Verbose "ESP drive letter: $SystemDriveLetter"
        } else {
            Write-Host "Preparing MBR System partition"
            $SystemPartition = $MountedVHDDisk | New-Partition -AssignDriveLetter -Size 350MB -MbrType IFS -IsActive
            $SystemPartition | Format-Volume -FileSystem NTFS -NewFileSystemLabel "System" | Out-Null
            $SystemDriveLetter = $SystemPartition.DriveLetter + ":\"
            Write-Verbose "MBR System drive letter: $SystemDriveLetter"
        }

        Write-Host "Preparing Windows partition"
        $WindowsPartition = $MountedVHDDisk | New-Partition -AssignDriveLetter -UseMaximumSize
        $WindowsPartition | Format-Volume -FileSystem NTFS -NewFileSystemLabel "Windows" | Out-Null
        $WindowsDriveLetter = $WindowsPartition.DriveLetter + ":\"
        Write-Verbose "Windows drive letter: $WindowsDriveLetter"

        Write-Host "Applying WIM image to partition..."
        Expand-WindowsImage -ImagePath $WIMPath -Index $ImageIndex -ApplyPath $WindowsDriveLetter | Out-Null

        $WindowsPath = Join-Path $WindowsDriveLetter "Windows"
        $BCDBootPath = Join-Path $Env:SystemRoot "System32\bcdboot.exe"
        $BCDBootArgs = "$WindowsPath /S $SystemDriveLetter"
        if ($PartitionStyle -EQ "MBR") {
            $BCDBootArgs += " /F BIOS"
        }

        Write-Host "Running bcdboot.exe to create BCD."
        Start-Process -Wait -WindowStyle Hidden -FilePath $BCDBootPath -ArgumentList $BCDBootArgs

        If ($UnattendXMLPath -NE "") {
            $PantherPath = Join-Path $WindowsPath "Panther\"
            mkdir $PantherPath | Out-Null
            Copy-Item $UnattendXMLPath $PantherPath | Out-Null
        }
    }
    Finally {
        Write-Host "Dismounting VHD..."
        Dismount-DiskImage -InputObject $MountedVHD | Out-Null
    }

    If ($NoSnapshot -EQ $false) {
        Write-Host "Creating snapshot..."
        $Snapshot = Checkpoint-VM -VM $VM -Passthru
        $SnapshotNewName = $Snapshot.Name + " - WIM Image applied"
        Rename-VMSnapshot -VM $VM -Name $Snapshot.Name -NewName $SnapshotNewName
    }

    Return $VM
}


Function New-VM-Impl-Wrapper {
    [CmdletBinding(PositionalBinding=$false)]
    Param (
    [Parameter(Mandatory = $True)][string]$Name,
    [Parameter(Mandatory = $True)][ValidateSet(1,2)][int]$Generation,
    [Parameter(Mandatory = $True)][int]$CPUCores,
    [Parameter(Mandatory = $True)][Int64]$MemorySize,
    [Parameter(Mandatory = $True)][Int64]$DiskSize,
    [Parameter(Mandatory = $True)][ValidateSet("GPT","MBR")][string]$PartitionStyle,
    [Parameter(Mandatory = $True)][AllowEmptyString()][string]$VMSwitchName,
    [Parameter(Mandatory = $True)][AllowEmptyString()][string]$UnattendXMLPath,
    [Parameter(ParameterSetName = 'ISOIndex', Mandatory = $True)]
    [Parameter(ParameterSetName = 'ISOName', Mandatory = $True)]
    [string]$ISOPath,
    [Parameter(ParameterSetName = 'WIMIndex', Mandatory = $True)]
    [Parameter(ParameterSetName = 'WIMName', Mandatory = $True)]
    [string]$WIMPath,
    [Parameter(ParameterSetName = 'ISOName', Mandatory = $True)]
    [Parameter(ParameterSetName = 'WIMName', Mandatory = $True)]
    [string]$ImageName,
    [Parameter(ParameterSetName = 'ISOIndex', Mandatory = $True)]
    [Parameter(ParameterSetName = 'WIMIndex', Mandatory = $True)]
    [int]$ImageIndex,
    [Parameter(Mandatory = $True)][Switch]$NoSnapshot,
    [Parameter(Mandatory = $True)][Switch]$Passthru
)
    
    $ArgsToImpl = $PSBoundParameters

    # Verify VMSwitch exists
    If ($VMSwitchName -NE "") {
        $VMSwitch = Get-VMSwitch | Where-Object -Property Name -EQ $VMSwitchName

        If ($null -EQ $VMSwitch) { 
            Throw "VMSwitch '$VMSwitchName' not found!"
        }
    }

    # Verify unattend.xml file exists
    If ($UnattendXMLPath -NE "") {
        If (!(Test-Path $UnattendXMLPath -PathType Leaf)) {
            Throw "File '$UnattendXMLPath' not found!"
        }
    }

    Write-Verbose "Cores: $CPUCores"
    Write-Verbose "RAM: $($MemorySize/1GB)GB"
    Write-Verbose "Disk: $($DiskSize/1GB)GB"
    Write-Verbose "Net: $VMSwitchName"
    Write-Verbose "unattend.xml path: $UnattendXMLPath"

    Try {
        
        If ($ISOPath -NE "") {
            Write-Host "Mounting ISO..."
            Write-Verbose "ISO file path: $ISOPath"
            $MountedISO = Mount-DiskImage -ImagePath $ISOPath

            $MountedDriveLetter = $MountedISO | Get-Volume | Select-Object -ExpandProperty DriveLetter
            $MountedDrivePath = $MountedDriveLetter + ":\"
            Write-Verbose "Mounted ISO as: $MountedDrivePath"
            
            $WIMPath = Join-Path $MountedDrivePath "sources\install.wim"
            $ArgsToImpl.Remove("ISOPath") | Out-Null
            $ArgsToImpl["WIMPath"] = $WIMPath
        }
        
        Write-Verbose "WIM file path: $WIMPath"
        
        If ($ImageName -NE "") {
            Write-Host "Looking for image '$ImageName' in WIM file."            
            $ImageIndex = Get-WindowsImage -ImagePath $WIMPath | Where-Object -Property ImageName -EQ $ImageName |
                            Select-Object -ExpandProperty ImageIndex
            IF ($ImageIndex -EQ 0) {
                Throw "Image '$ImageName' not found in WIM file."
            }
            
            $ArgsToImpl.Remove("ImageName") | Out-Null
            $ArgsToImpl["ImageIndex"] = $ImageIndex
        }
        
        Write-Verbose "Using WIM index: $ImageIndex."
        $VM = New-VM-Impl @ArgsToImpl

        Return $VM
    }
    Finally {
        if (Test-Path Variable:MountedISO) {
            Write-Host "Dismounting ISO..."
            Dismount-DiskImage -InputObject $MountedISO | Out-Null
        }
    }

}


#### Main Script starts here

$ArgsToImplWrapper = $PSBoundParameters

foreach($p in $MyInvocation.MyCommand.Parameters.GetEnumerator()) {
    If ($p.Value.ParameterSets.ContainsKey($PSCmdlet.ParameterSetName) -or
        $p.Value.ParameterSets.ContainsKey("__AllParameterSets")) {
        Try {
            $key = $p.Key
            $val = Get-Variable -Name $key -ErrorAction Stop | Select-Object -ExpandProperty Value -ErrorAction Stop
            if (([String]::IsNullOrEmpty($val) -and (!$PSBoundParameters.ContainsKey($key)))) {
                Write-Debug "[PARAM] A blank value that wasn't supplied by the user."
            }
            Write-Debug "[PARAM] $key => '$val'"
            $ArgsToImplWrapper[$key] = $val
        } Catch {}
    }
}

$VM = New-VM-Impl-Wrapper @ArgsToImplWrapper

Return $VM
